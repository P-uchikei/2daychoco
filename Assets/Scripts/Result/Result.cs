﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Result : MonoBehaviour {

    MainGameManager MGMScript;

    private MoveScene MSScript;

    public Image character;
    //[0]-[2]はおじさん、[3]-[5]はボーイ、[6]-[8]はガールの仮想二次元配列
    //それぞれ[0]はSmile、[1]はNormal、[2]はAngry
    public Sprite[] charImages = new Sprite[9];
    //[0]は背景イメージ
    public Image[] image = new Image[1];
    //[0]はメニュー移行ボタン
    public Button[] button = new Button[1];
    //メニュー移行ボタンテキスト
    public Text buttonText;
    //[0]は
    public Text[] text = new Text[1];

    //スコアから算出されるランク
    //0→Smile, 1→Normal 2→Angry
    int rank = 0;
    //ランクへと繋がる減算式で計算していくスコア
    int score = 100;

    //MainGameManagerのorder配列型変数に保存された値が引数となっている
    //甘さ
    int[] correctSweetness = { 15, 50, 100 };
    //大きさ
    float[] correctSize = { 3, 5, 10 };

    //オーダーと異なる時に減算されるスコア
    public int[] disSweetDiff = { 0, 10, 20 };
    public int[] disSizeDiff = { 0, 10, 20 };
    public int disMoldDiff = 10;

    //テキストUIをセットする時にランクの値が計算された後かどうかを判断する真偽値
    private bool isSeted = false;

	// Use this for initialization
	void Start () {
        Debug.Log("result");
        MGMScript = GetComponent<MainGameManager>();
        MSScript = GetComponent<MoveScene>();

        character.sprite = charImages[MGMScript.GetCharacter() * 3 + GetRank()];

        //ランクごとの効果音を鳴らす
        MGMScript.PSScript.playOneTime(MGMScript.audioS, MGMScript.clip[5 + GetRankNum()]);

        Debug.Log("Rank:" + GetRank());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //テキストUIへランクの結果を送るための関数
    public int GetRankNum(){
        return GetRank();
    }
    public bool GetIsSeted(){
        return isSeted;
    }
    //メニュー移行ボタンが押された時の処理
    public void ReturnButtonPushed(){
        //ボタンを押した効果音を鳴らす
        MGMScript.PSScript.playOneTime(MGMScript.audioS, MGMScript.clip[0]);

        MSScript.callScene("Title");
    }

    //人物、背景、ボタン、テキストの表示非表示
    private void SetImageEnabled(bool check){
        character.enabled = check;
        for (int i = 0; i < image.Length; i++){
            image[i].enabled = check;
        }
        for (int i = 0; i < button.Length; i++){
            button[i].image.enabled = check;
        }
        buttonText.enabled = check;
        for (int i = 0; i < text.Length; i++){
            text[i].enabled = check;
        }
    }

    //最終的なスコアを計算し、そのスコアからランクを決める
    private int GetRank(){
        //スコアの計算
        score = score - GetDisSweetScore() - GetDisSizeScore() - GetDisMoldScore();
        //セットされたフラグを立てる
        isSeted = true;

        if (score >= 90) return 0;
        else if (score >= 70) return 1;
        else return 2;
    }
    //甘さの結果と正解の差異から減算する分のスコアを計算して返す関数
    private int GetDisSweetScore(){
        //MainGameManagerの最終的なsweetnessをセット
        int ResultSweetness = MGMScript.GetSweetness();

        //結果と正解との差異から減算するスコアを計算
        if (Mathf.Abs(ResultSweetness - correctSweetness[MGMScript.GetOrder(0)]) <= 10)
        {
            return disSweetDiff[0];
        }
        else if (Mathf.Abs(ResultSweetness - correctSweetness[MGMScript.GetOrder(0)]) <= 30)
        {
            return disSweetDiff[1];
        }
        else return disSweetDiff[2];
    }
    //大きさの結果と正解の差異から減算する分のスコアを計算して返す関数
    private int GetDisSizeScore(){
        //MainGameManagerの最終的なSizeをセット
        //差異の計算のためにfloat型から*100後にint型へ四捨五入
        int ResultSize = Mathf.RoundToInt(MGMScript.GetSize() * 100);

        //結果と正解との差異から減算するスコアを計算
        if (Mathf.Abs(ResultSize - correctSize[MGMScript.GetOrder(1)]) <= 10)
        {
            return disSizeDiff[0];
        }
        else if (Mathf.Abs(ResultSize - correctSize[MGMScript.GetOrder(1)]) <= 30)
        {
            return disSizeDiff[1];
        }
        else return disSizeDiff[2];
    }
    //型の結果と正解の差異から減算する分のスコアを計算して返す関数
    private int GetDisMoldScore(){
        //MainGameManagerの最終的なMoldをセット
        int ResultMold = MGMScript.GetMold();

        //結果と正解との差異から減算するスコアを計算
        if (ResultMold == MGMScript.GetOrder(2))
        {
            return 0;
        }
        else return disMoldDiff;
    }

    //工程の1つとしてMainGameManagerで呼ばれる関数
    public void ResultMethod(){
        SetImageEnabled(true);
    }
}

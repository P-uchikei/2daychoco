﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankTextScript : MonoBehaviour {

    Result ResultScript;

    bool check = false;

	// Use this for initialization
	void Start () {
        ResultScript = Camera.main.GetComponent<Result>();

	}
	
	// Update is called once per frame
	void Update () {
        if(!check && ResultScript.GetIsSeted()){
            //リザルト画面のランクテキストをセット
            //this.GetComponent<Text>().text = ResultScript.GetRankNum().ToString();
            if(ResultScript.GetRankNum() == 0){
                this.GetComponent<Text>().text = "S";
            }
            else if(ResultScript.GetRankNum() == 1){
                this.GetComponent<Text>().text = "A";
            }
            else if(ResultScript.GetRankNum() == 2){
                this.GetComponent<Text>().text = "B";
            }

            check = true;
        }
	}
}

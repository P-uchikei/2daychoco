﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGameManager : MonoBehaviour {

    Order OrderScript;
    SelectChocolateType SCTScript;
    public DetermineSize DSScript;
    AddSugar ASScript;
    AddMilk AMScript;
    SelectChocolateMold SCMScript;
    StopTimer STScript;
    Result ResultScript;
    public PlaySound PSScript;

    public AudioSource audioS;
    public AudioClip[] clip = new AudioClip[8];

    public int process = 2;

    //オーダー
    //キャラクター,0はおじさん、1はボーイ、2はガール
    private int character = 0;
    //[0]は甘さ、[1]は大きさ、[2]は型
    private int[] order = {0, 0, 0};

    //チョコレートの種類を決める変数
    //0がビター,1がミルク,2がホワイト
    private int chocoType = 0;
    //甘さを決める変数
    private int sweetness = 0;
    //大きさを決める変数
    private float size = 0.0f;
    //形を決める変数
    private int mold = 0;
    //冷やした時間を決める変数
    private float time = 0.0f;


	// Use this for initialization
	void Start () {
        OrderScript = GetComponent<Order>();
        SCTScript = GetComponent<SelectChocolateType>();
        //DSScript = GetComponent<DetermineSize>();
        ASScript = GetComponent<AddSugar>();
        AMScript = GetComponent<AddMilk>();
        SCMScript = GetComponent<SelectChocolateMold>();
        STScript = GetComponent<StopTimer>();
        ResultScript = GetComponent<Result>();

	}
	
	// Update is called once per frame
	void Update () {
        OrderScript.OrderMethod();
        //工程0,お客さんからのオーダー処理
        if(process == 0){
            //OrderScript.OrderMethod();
        }
        //工程1,チョコレートの種類の選択
        else if(process == 1){
            SCTScript.STCMethod(true);

        }
        //工程2,チョコレートの量(大きさ)を決定する
        else if(process == 2){
            DSScript.DSMethod();
        }
        //工程3,砂糖を入れて甘さを調節
        else if(process == 3){
            ASScript.ASMethod();
        }
        //工程4,MILKを入れて甘さを調節
        else if(process == 4){
            AMScript.AMMethod();
        }
        //工程5,チョコレートの型を選択
        else if(process == 5){
            SCMScript.SCMMethod(true);
        }
        //工程6,冷やす時間を決める
        else if(process == 6){
            STScript.STMethod();
        }
        //工程7,結果の判定
        else if(process == 7){
            ResultScript.ResultMethod();
        }
	}

    //工程を進める関数
    public void AddProcess(int addNum){
        process += addNum;
    }
    public int GetCharacter(){
        return character;
    }
    public void SetCharacter(int charNum){
        character = charNum;
    }
    public int GetOrder(int num){
        return order[num];
    }
    public void SetOrder(int typeNum, int orderNum){
        order[typeNum] = orderNum;
    }
    public int GetChocoType(){
        return chocoType;
    }
    public void SetChocoType(int typeNum){
        chocoType = typeNum;
    }
    public int GetSweetness(){
        return sweetness;
    }
    public void AddSweetness(int addNum){
        sweetness += addNum;
    }
    public float GetSize(){
        return size;
    }
    public void SetSize(float setNum){
        size = setNum;
    }
    public int GetMold(){
        return mold;
    }
    public void SetMold(int moldType){
        mold = moldType;
    }
    public float GetTime(){
        return time;
    }
    public void SetTime(float setTime){
        time = setTime;
    }

}

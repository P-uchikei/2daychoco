﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DetermineSize : MonoBehaviour {

    MainGameManager MGMScript;

    //カットされるチョコレートのイメージ
    public Image chocolatePlane;
    //それぞれ[0]はビターチョコ、[1]はミルクチョコ、[2]はホワイトチョコ
    public Sprite[] chocoImages = new Sprite[3];

    //チョコレートの絵柄を映すためのイメージ
    //[0]が包丁、[1]がカットするときの目安のライン
    public Image[] cutLine = new Image[2];

    //目安の位置は真ん中からスタート
    float time = 0.5f;
    //目安ラインのUIが右方向へ移動しているかどうかの真偽値
    bool isRightDirection = true;

    //クリックされてから遷移するまでの時間を測る変数
    float transitionTime = 0.0f;
    bool flug = true;

    bool trigger = false;

	// Use this for initialization
	void Start () {
        MGMScript = GetComponent<MainGameManager>();

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void SetImageEnabled(bool check){
        //チョコのイメージ
        chocolatePlane.enabled = check;
        //包丁・目安ラインのイメージ
        for (int i = 0; i < cutLine.Length; i++){
            cutLine[i].enabled = check;
        }
    }

    public void SetImage(){
        chocolatePlane.sprite = chocoImages[MGMScript.GetChocoType()];
    }

    public void DSMethod(){
        //UIイメージを表示させる
        SetImageEnabled(true);
        //クリックされていない間
        if(!Input.GetMouseButtonDown(0) && transitionTime == 0.0f){
            //右端にたどり着いたとき
            if(time >= 1){
                isRightDirection = false;
            }
            //左端にたどり着いたとき
            else if(time <= 0){
                isRightDirection = true;
            }

            if(isRightDirection){
                time += Time.deltaTime;
            }
            else{
                time -= Time.deltaTime;
            }

            //チョコレートの幅は523
            //一番左の位置-261.5 に横の長さ523 のtime(現在の比率)分かけて現在の位置を計算する
            cutLine[0].transform.localPosition = new Vector3(-261.5f + 523.0f * time + 70, 0, 0);
            cutLine[1].transform.localPosition = new Vector3(-261.5f + 523.0f * time, 0, 0);
        }
        else{
            transitionTime += Time.deltaTime;
            //クリックされたら一度だけチョコの大きさデータをGameManagerに送っておく
            if(flug){
                //クリックしたとき効果音を鳴らす
                MGMScript.PSScript.playOneTime(MGMScript.audioS, MGMScript.clip[1]);

                MGMScript.SetSize(time);
                flug = false;
            }

            //時間が一定時間経過したらUIを消して工程を1つ進める
            if(transitionTime >= 2.0f){
                SetImageEnabled(false);
                MGMScript.AddProcess(1);
            }
        }
    }
}

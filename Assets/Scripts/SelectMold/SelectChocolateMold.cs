﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectChocolateMold : MonoBehaviour {

    MainGameManager MGMScript;

    //チョコレートの絵柄を映すためのイメージ
    //[0]は星型、[1]はハート型、[2]はくまちゃん型
    public Image[] icon = new Image[3];

    //イメージアイコンが押され済みかどうかを保存する変数
    private bool isClicked = false;

    private string imageName = "";

    //次の工程に遷移するまでの時間を測る変数
    float transitionTime = 0.0f;

	// Use this for initialization
	void Start () {
        MGMScript = GetComponent<MainGameManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void SetImageEnabled(bool check){
        for (int i = 0; i < icon.Length; i++){
            icon[i].enabled = check;
        }
    }

    //イメージアイコンのクリック状態を管理する関数
    public void SetIsClicked(bool check){
        //ボタンを押した効果音を鳴らす
        MGMScript.PSScript.playOneTime(MGMScript.audioS, MGMScript.clip[0]);

        isClicked = check;
    }

    public string GetImageName(){
        return imageName;
    }
    //クリックされたイメージの名前をセット
    public void SetImageName(string name){
        if(isClicked){
            imageName = name;

            //クリックされたイメージによって甘さを追加
            switch (GetImageName())
            {
                //星型を選択したとき
                case "BitterMold":
                    MGMScript.SetMold(0);
                    break;
                //ハート型を選択したとき
                case "MilkMold":
                    MGMScript.SetMold(1);
                    break;
                //くまちゃん型を選択したとき
                case "BearMold":
                    MGMScript.SetMold(2);
                    break;
                default:
                    MGMScript.SetMold(0);
                    break;
            }
        }
    }


    //工程の１つとしてGameManagerで呼ばれる関数
    public void SCMMethod(bool active)
    {
        //イメージアイコンをスタート時は表示させる、終了時は非表示にする
        SetImageEnabled(true);

        //イメージアイコンが表示されている場合
        if (active)
        {
            //イメージアイコンがクリックされたとき
            if (isClicked)
            {
                transitionTime += Time.deltaTime;

                //遷移するまでの制限時間を超えたら
                if(transitionTime >= 2.0f){
                    Debug.Log(GetImageName());

                    //終了時はイメージアイコンを非表示にする
                    SetImageEnabled(false);

                    //工程を１つ進める
                    MGMScript.AddProcess(1);
                }
            }
        }
    }
}

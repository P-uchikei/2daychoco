﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectMoldEvent : MonoBehaviour {
    
    private Image image;

    //imageアイコンがクリックされたときに呼ばれる処理
    public void MyPointerDownUI()
    {
        Camera.main.GetComponent<SelectChocolateMold>().SetIsClicked(true);
        image = GetComponent<Image>();
        Camera.main.GetComponent<SelectChocolateMold>().SetImageName(image.name);
        Debug.Log("Selceted Mold.");
    }

}

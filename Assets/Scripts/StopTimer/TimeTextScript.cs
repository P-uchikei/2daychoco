﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeTextScript : MonoBehaviour {

    StopTimer STScript;

	// Use this for initialization
	void Start () {
        STScript = Camera.main.GetComponent<StopTimer>();

	}
	
	// Update is called once per frame
	void Update () {
        this.GetComponent<Text>().text = STScript.GetTime().ToString("f2");
	}
}

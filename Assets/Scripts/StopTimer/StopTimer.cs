﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StopTimer : MonoBehaviour {

    MainGameManager MGMScript;

    //[0]はキッチンタイマー
    public Image[] icon = new Image[1];
    //[0]はストップボタン
    public Button[] button = new Button[2];
    //[0]はストップボタンのテキスト
    public Text[] text = new Text[2];
    //時間を表示するテキスト
    public Text timeText;

    //現在の経過時間を測る変数(UIテキストなどに使用)
    private float time = 0.0f;
    //止める時に合わせる規定の時間
    public float prescriptedTime = 10.0f;

    //次の工程に遷移するまでの時間を測る変数
    float transitionTime = 0.0f;

    //スタートボタンが押されたかどうかの真偽値
    private bool startPushed = false;
    //ストップボタンが押されたかどうかの真偽値
    private bool finishPushed = false;

	// Use this for initialization
	void Start () {
        MGMScript = GetComponent<MainGameManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //イメージ、ストップボタン、時間テキストの表示非表示
    private void SetImageEnabled(bool check){
        for (int i = 0; i < icon.Length; i++){
            icon[i].enabled = check;
        }
        for (int i = 0; i < button.Length; i++){
            button[i].image.enabled = check;
        }
        for (int i = 0; i < text.Length; i++){
            text[i].enabled = check;
        }
        timeText.enabled = check;
    }

    //スタートボタンUIがクリックされたときの関数
    public void StartButtonPushed(){
        //まだスタートボタンがクリックされていないときだけ流れる処理
        if(!startPushed){
            //ボタンを押すけども、ストプトップウォッチを止める効果音を鳴らす
            MGMScript.PSScript.playOneTime(MGMScript.audioS, MGMScript.clip[4]);

            startPushed = true;
        }
    }
    //ストップボタンUIがクリックされた時の関数
    public void StopButtonPushed(){
        //スタートボタンがクリックされたあとだけ流れる処理
        if(startPushed){
            //ボタンを押すけども、ストプトップウォッチを止める効果音を鳴らす
            MGMScript.PSScript.playOneTime(MGMScript.audioS, MGMScript.clip[4]);

            finishPushed = true;
        }
    }
    //時間テキスト表示用関数
    public float GetTime(){
        return time;
    }

    //工程の１つとしてMainGameManagerで呼ばれる関数
    public void STMethod(){
        SetImageEnabled(true);

        //スタートボタンが押されたあとの処理
        if(startPushed){
            //ストップボタンがまだクリックされていないときの処理
            if (!finishPushed){
                time += Time.deltaTime;
            }
            //ストップボタンがクリックされたあとの処理
            else{
                transitionTime += Time.deltaTime;

                //一定時間後にUIを非表示にして、工程を1つ進め画面を遷移
                if (transitionTime >= 3.0f){
                    SetImageEnabled(false);
                    MGMScript.SetTime(time);
                    MGMScript.AddProcess(1);
                }
            }
        }
    }
}

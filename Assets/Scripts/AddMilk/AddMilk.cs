﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddMilk : MonoBehaviour {

    MainGameManager MGMScript;

    //[0]はMILKイメージ、[1]はボウルイメージ
    public Image[] icon = new Image[2];
    //[0]は追加するボタン、[1]は終了ボタン
    public Button[] button = new Button[2];
    //[0]は追加するテキスト、[1]は終了テキスト
    public Text[] text = new Text[2];

    //制限時間までの時間を測る変数
    float time = 0.0f;
    //MILKを追加できる制限時間
    public float limitTime = 6.0f;

    //MILKを追加するボタンを押した回数を保存
    private int clickCount = 0;
    //MILKを追加できる回数の制限
    public int limitCount = 5;

    //終了してから遷移するまでの時間を測る変数
    float transitionTime = 0.0f;


	// Use this for initialization
	void Start () {
        MGMScript = GetComponent<MainGameManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void AddClickCount(){
        clickCount++;
        Debug.Log(MGMScript.GetSweetness());
    }

    //MILKとボウル、追加・終了ボタン、ボタンテキストの表示非表示
    private void SetImageEnabled(bool check){
        for (int i = 0; i < icon.Length; i++){
            icon[i].enabled = check;
        }
        for (int i = 0; i < button.Length; i++){
            button[i].image.enabled = check;
        }
        for (int i = 0; i < text.Length; i++){
            text[i].enabled = check;
        }
    }

    //MILKを追加するボタンが押されたとき、GameManagerの甘さ値を1加し、クリック回数を増加
    public void AddMilkButtonPushed(){
        if (clickCount < limitCount){
            //ミルクを追加した効果音を鳴らす
            MGMScript.PSScript.playOneTime(MGMScript.audioS, MGMScript.clip[3]);

            MGMScript.AddSweetness(1);
            AddClickCount();
        }
    }
    //終了するボタンが押されたとき、制限時間を超えさせ、遷移までの時間を1秒短くする
    public void FinishButtonPushed(){
        //ボタンを押した効果音を鳴らす
        MGMScript.PSScript.playOneTime(MGMScript.audioS, MGMScript.clip[0]);

        time = limitTime + 1.0f;
        transitionTime = 1.0f;
    }

    public void AMMethod(){
        SetImageEnabled(true);

        //時間が制限時間を超えていない、または、クリックした(砂糖を追加)回数が制限回数を超えていないとき
        if (time <= limitTime && clickCount < limitCount){
            time += Time.deltaTime;

        }
        else{ //次の工程に移るとき
            transitionTime += Time.deltaTime;

            //遷移の制限時間まで経過したとき
            if (transitionTime >= 2.0f){
                SetImageEnabled(false);
                MGMScript.AddProcess(1);
            }
        }
    }

}

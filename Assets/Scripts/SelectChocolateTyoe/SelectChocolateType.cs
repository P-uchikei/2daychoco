﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//第１工程でチョコレートの種類を決める際のスクリプト
public class SelectChocolateType : MonoBehaviour {

    MainGameManager MGMScript;

    //チョコレートの絵柄を映すためのイメージ
    //[0]はビター、[1]はミルク、[2]はホワイト
    public Image[] icon = new Image[3];

    //イメージアイコンが押され済みかどうかを保存する変数
    private bool isClicked = false;

    private string imageName = "";

    //次の工程に遷移するまでの時間を測る変数
    float transitionTime = 0.0f;

	// Use this for initialization
	void Start () {
        MGMScript = GetComponent<MainGameManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void SetImageEnabled(bool check){
        for (int i = 0; i < icon.Length; i++){
            icon[i].enabled = check;
        }
    }

    //工程の1つとしてGameManagerで呼ばれる関数
    public void STCMethod(bool active){
        //イメージアイコンをスタート時は表示させる、終了時は非表示にする
        SetImageEnabled(active);

        //イメージアイコンが表示されている場合
        if(active){
            //イメージアイコンがクリックされたとき
            if(isClicked){

                transitionTime += Time.deltaTime;

                //
                MGMScript.DSScript.SetImage();

                //遷移するまでの制限時間を超えたら
                if(transitionTime >= 2.0f){
                    Debug.Log(imageName);

                    //終了時はイメージアイコンを非表示にする
                    SetImageEnabled(false);

                    //工程を１つ進める
                    MGMScript.AddProcess(1);

                }
            }
        }
    }

    //イメージアイコンのクリック状態を管理する関数
    public void SetIsClicked(bool check){
        //ボタンを押した効果音を鳴らす
        MGMScript.PSScript.playOneTime(MGMScript.audioS, MGMScript.clip[0]);

        isClicked = check;
    }

    public string GetImageName(){
        return imageName;
    }
    //クリックされたイメージの名前をセット
    public void SetImageName(string name){
        if(isClicked){
            imageName = name;

            //クリックされたイメージによって甘さを追加
            switch (GetImageName())
            {
                //ビターを選択したとき
                case "Bitter":
                    Debug.Log("bitter");
                    MGMScript.SetChocoType(0);
                    MGMScript.AddSweetness(0);
                    break;
                //ミルクを選択したとき
                case "Milk":
                    Debug.Log("milk");
                    MGMScript.SetChocoType(1);
                    MGMScript.AddSweetness(50);
                    break;
                //ホワイトチョコを選択したとき
                case "White":
                    MGMScript.SetChocoType(2);
                    MGMScript.AddSweetness(70);
                    break;
                default:
                    //MGMScript.AddSweetness(0);
                    break;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReceiveEvent : MonoBehaviour {

    //public GameObject obj;
    //public MainGameManager script;

    private Image image;

    //imageアイコンがクリックされたときに呼ばれる処理
    public void MyPointerDownUI()
    {
        Camera.main.GetComponent<SelectChocolateType>().SetIsClicked(true);
        image = GetComponent<Image>();
        Camera.main.GetComponent<SelectChocolateType>().SetImageName(image.name);
    }
}

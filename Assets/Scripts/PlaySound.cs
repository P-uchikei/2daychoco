﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySound : MonoBehaviour
{
    private AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {

    }


    // BGM
    public void playStart()
    {
        audioSource.Play();
    }

    public void playStop()
    {
        audioSource.Stop();
    }


    //  SE
    public void playOneTime(AudioSource audio, AudioClip clip)
    {
        audio.PlayOneShot(clip);
    }
}
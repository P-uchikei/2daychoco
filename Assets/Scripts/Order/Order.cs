﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Order : MonoBehaviour {
    
    MainGameManager MGMScript;

    //[0]はおじさん
    public Image[] character = new Image[1];
    //[0]-[2]はおじさん、[3]-[5]はボーイ、[6]-[8]はガールの仮想二次元配列
    //それぞれ[0]はSmile、[1]はNormal、[2]はAngry
    public Sprite[] charImages = new Sprite[9];
    //[0]は背景イメージ
    public Image[] image = new Image[1];
    //[0]はゲームスタートボタン
    public Button[] button = new Button[1];
    //ゲームスタートボタンテキスト
    public Text startText;
    //[0]は要望テキスト、[1]は甘さ、[2]は大きさ、[3]は型
    public Text[] text = new Text[4];

    //[0,]が甘さ、[1,]が大きさ、[2,]が型
    string[,] keyword = { { "甘くない", "甘すぎない", "甘い"},
                            { "小さい", "普通くらいの", "大きい"}, 
                            { "ゴージャス", "ラブリー", "プリティー"} };

    //大きさの規定値
    int[] size = { 3, 5, 10 };

    //オーダーが入力されたかどうかを判定する真偽値
    private bool isSeted = false;

    //チョコ作りスタートボタンが押されたかどうかを判断する真偽値
    bool isComfirmed = false;
    //オーダーUIが移動済みかどうかを判断する真偽値
    bool isUIMoved = false;

	// Use this for initialization
	void Start () {
        MGMScript = GetComponent<MainGameManager>();

        //どの人物が出てくるかをランダムで選択
        MGMScript.SetCharacter(Random.Range(0, 3));
        //表示されるイメージを配列の中から選択(表情はNormal)
        character[0].sprite = charImages[MGMScript.GetCharacter() * 3 + 1];

        //ランダムでオーダー(甘さ、大きさ、型)を決める
        for (int i = 0; i < keyword.GetLength(0); i++){
            //[0]甘さ、[1]大きさ　あらかじめ決められたサイズsize[]の中からランダムで決める、[2]型
            MGMScript.SetOrder(i, Random.Range(0, 3));
            //キーワードテキストをバインドする瞬間の方が早くなっているため、フラグを用意
            SetIsSeted(true);
            //text[i + 1].text = keyword[i, MGMScript.GetOrder(i)];
            Debug.Log(i + " : " + MGMScript.GetOrder(i));
        }

        SetImageEnabled(true);
        //text[0].text = keyword[0, MGMScript.GetOrder(0)] + "もの、" +
            //keyword[1, MGMScript.GetOrder(1)] + "大きさがちょうど良くて、" +
            //keyword[2, MGMScript.GetOrder(2)] + "なチョコレートがほしいなぁ";

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public string GetKeyword(int num1, int num2){
        return keyword[num1, num2];
    }
    public bool GetIsSeted(){
        return isSeted;
    }
    public void SetIsSeted(bool check){
        isSeted = check;
    }

    //チョコ作りスタートボタンが押された時の処理
    public void StartButtonPushed(){
        //音鳴らす♪
        MGMScript.PSScript.playOneTime(MGMScript.audioS, MGMScript.clip[0]);

        //ボタンとそのテキストの非表示
        button[0].image.enabled = false;
        startText.enabled = false;
        //チョコ作りスタートボタンが押されたことを確認
        isComfirmed = true;
        //工程を1つ進める
        MGMScript.AddProcess(1);
    }

    //人間。背景、ボタン、テキストの表示非表示
    private void SetImageEnabled(bool check){
        for (int i = 0; i < character.Length; i++){
            character[i].enabled = check;
        }
        for (int i = 0; i < image.Length; i++){
            image[i].enabled = check;
        }
        for (int i = 0; i < button.Length; i++){
            button[i].image.enabled = check;
        }
        startText.enabled = check;
        for (int i = 0; i < text.Length; i++){
            text[i].enabled = check;
        }
    }


    public void OrderMethod(){
        //チョコ作りスタートボタンがクリックされた、かつ、UIがまだ動いていない
        if(isComfirmed && !isUIMoved){
            image[0].transform.localPosition = new Vector3(0, 250, 0);
            isUIMoved = true;
        }

    }


}

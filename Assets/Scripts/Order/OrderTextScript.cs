﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OrderTextScript : MonoBehaviour {

    MainGameManager MGMScript;
    Order OrderScript;

	// Use this for initialization
	void Start () {
        MGMScript = Camera.main.GetComponent<MainGameManager>();
        OrderScript = Camera.main.GetComponent<Order>();

        this.GetComponent<Text>().text = 
            "    " + 
            "もの、\n\n" +
            "        " + 
            "サイズがちょうど良くて、\n\n" +
            "      " + 
            "なチョコレートがほしいなぁ";
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoldTextOrderText : MonoBehaviour {

    MainGameManager MGMScript;
    Order OrderScript;

    //テキストがバインドされたかどうかを判断する真偽値
    bool isBinded = false;

	// Use this for initialization
	void Start () {
        MGMScript = Camera.main.GetComponent<MainGameManager>();
        OrderScript = Camera.main.GetComponent<Order>();

	}
	
	// Update is called once per frame
	void Update () {
        //型テキストのバインド
        if (OrderScript.GetIsSeted() && !isBinded){
            this.GetComponent<Text>().text =
                OrderScript.GetKeyword(2, MGMScript.GetOrder(2));

            isBinded = true;
        }

	}
}
